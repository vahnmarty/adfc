function initMap() {
	var mapOptions = {
		center: {lat: 24.4285759, lng: 54.4613093},
		zoom: 12,
		scrollwheel: false,
		// styles: [{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]
		};
	var map = new google.maps.Map(document.getElementById('contactUsMap'), mapOptions);
	var marker = new google.maps.Marker({
		position: {lat: 24.4285759, lng: 54.4613093},
		animation: google.maps.Animation.BOUNCE,
		icon: {
			url: 'images/map-marker.png'
		},
		map: map,
		url: 'https://www.google.ae/maps/place/twofour54+Abu+Dhabi/@24.4285759,54.4613093,17z/data=!3m1!4b1!4m5!3m4!1s0x3e5e4244479a103f:0xa04bdff75c267f7a!8m2!3d24.428571!4d54.463498?hl=en'
	});
	google.maps.event.addListener(marker, 'click', function() {
	window.open(this.url, '_blank');
	});
}

initMap();