jQuery(function($) {

	
	initToggleMenu();
	//initHeaderAfix();
	initVideoModal();
	//animateHome();

	function initToggleSearch() {
		$(document).on('click', '.header-nav__search__icon--open', function(e) {
			e.preventDefault();
			$('.header-nav__menu').addClass('hide');
			$('.header-nav').addClass('search-is-open');
			$('.header-nav__search__form').addClass('opened').find('input[type="search"]').focus();
			$(this).addClass('header-nav__search__icon--close').removeClass('header-nav__search__icon--open');
		});

		$(document).on('click', '.header-nav__search__icon--close', function(e) {
			e.preventDefault();
			$('.header-nav__search__form').removeClass('opened');
			setTimeout(function() {
				$('.header-nav').removeClass('search-is-open');
				$('.header-nav__menu').removeClass('hide');
			}, 300);
			$(this).addClass('header-nav__search__icon--open').removeClass('header-nav__search__icon--close');
		});
	}

	function initToggleMenu() {
		$(document).on('click', '.header-nav__open-menu', function(e) {			
			e.preventDefault();
			$('.announcement').toggleClass('opened');		
			$('.header-nav__menu-wrapper').toggleClass('opened');
			$('.menu-icon').toggleClass('closed');
		});
	}

	function initHeaderAfix() {
		if ($(this).scrollTop() > 0) {
			$('.header-nav').addClass('scrolled');
		}

		$(window).on('scroll', function(e) {
			$('.header-nav').addClass('scrolled');
			if ($(this).scrollTop() === 0) {
				$('.header-nav').removeClass('scrolled');
			}
		});
	}

	function initVideoModal() {
		$('#myModal').on('show.bs.modal', function (e) {
			$(this).find('iframe').attr('src', "https://www.youtube.com/embed/BFGYs9XpQSM?rel=0&autoplay=1");
		});

		$('#myModal').on('hide.bs.modal', function (e) {
			$(this).find('iframe').attr('src', "");
		});
	}

	function animateHome() {
		$('.banner__home__text').addClass('animate');
		$('.service__box').each(function(i) {
			setTimeout(function() {
				$('.service__box').eq(i).addClass('showing');
			}, 350 * (i+1));
		});
	}

});