var gulp = require('gulp');  
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var livereload = require('gulp-livereload');
var cleanCSS = require('gulp-clean-css');

/* Scripts task */
gulp.task('scripts', function() {
  return gulp.src('js/*.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest('build/js/'))
});

/* Styles task */
gulp.task('styles', function () {  
    gulp.src(['sass/main.scss'])
    .pipe(plumber())
    .pipe(sass())
    .pipe(concat('style.css'))
    .pipe(gulp.dest(''))
});

gulp.task('watch', function () {
	var server = livereload();

    gulp.watch(['sass/**/*.scss'], ['styles'])
    gulp.watch(['js/*.js'], ['scripts'])
});

gulp.task('default', ['scripts', 'styles', 'watch'])